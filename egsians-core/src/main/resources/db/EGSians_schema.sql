create table if not exists hash_tags
(
    id   bigint auto_increment
        primary key,
    text varchar(255) null,
    constraint UK_o4eu6vpm2vt7tb98ab2xd4hj9
        unique (text)
);

create table if not exists users
(
    id         bigint auto_increment
        primary key,
    email      varchar(255) not null,
    full_name  varchar(255) not null,
    password   varchar(255) not null,
    first_name varchar(255) not null,
    last_name  varchar(255) not null,
    constraint UK_6dotkott2kjsp8vw4d0m25fb7
        unique (email)
);

create table if not exists friendship
(
    status    varchar(255) null,
    user_id   bigint       not null,
    friend_id bigint       not null,
    primary key (friend_id, user_id),
    constraint FK3xu72n4ht9qij4qc7wdpb2whd
        foreign key (friend_id) references users (id)
            on delete cascade,
    constraint FKgc658l9wcttfpk0c1d3englh9
        foreign key (user_id) references users (id)
            on delete cascade
);

create index friendship_friend_id_IDX
    on friendship (friend_id);

create index friendship_user_id_IDX
    on friendship (user_id);

create table if not exists posts
(
    id      bigint auto_increment
        primary key,
    date    datetime(6)   null,
    text    varchar(1000) not null,
    user_id bigint        null,
    constraint FK5lidm6cqbc7u4xhqpxm898qme
        foreign key (user_id) references users (id)
);

create table if not exists hash_tags_posts
(
    post_id     bigint not null,
    hash_tag_id bigint not null,
    constraint FK4a657g8yw6oor3na6c06x4h8y
        foreign key (post_id) references posts (id)
            on delete cascade,
    constraint FK8206nthf8vwt1jc85c0kqfyx
        foreign key (hash_tag_id) references hash_tags (id)
            on delete cascade
);

create table if not exists images
(
    id         bigint auto_increment
        primary key,
    image_path varchar(255) not null,
    post_id    bigint       null,
    constraint UK_aany1wtqlp14f271icr6yq4x6
        unique (image_path),
    constraint FKcp0pycisii8ub3q4b7x5mfpn1
        foreign key (post_id) references posts (id)
);

create index images_id_IDX
    on images (id);

create table if not exists post_comments
(
    id           bigint auto_increment
        primary key,
    post_comment varchar(1000) null,
    time         datetime(6)   null,
    post_id      bigint        null,
    user_id      bigint        null,
    constraint FKaawaqxjs3br8dw5v90w7uu514
        foreign key (post_id) references posts (id)
            on delete cascade,
    constraint FKsnxoecngu89u3fh4wdrgf0f2g
        foreign key (user_id) references users (id)
            on delete cascade
);

create table if not exists post_comments_reply
(
    comment_id bigint not null,
    reply_id   bigint not null,
    constraint UK_lswce64ii0fes8krodjr5cuck
        unique (reply_id),
    constraint FK7jfdxhnk3wkroejus3nyijwjb
        foreign key (reply_id) references post_comments (id)
            on delete cascade,
    constraint FKpxm9we1qvuu57d7q7ro2getkm
        foreign key (comment_id) references post_comments (id)
            on delete cascade
);

create index posts_id_IDX
    on posts (id);

create index posts_user_id_IDX
    on posts (user_id);

create table if not exists user_bio
(
    id            bigint auto_increment
        primary key,
    birthday      datetime(6)   null,
    image_path    varchar(255)  null,
    mobile_number varchar(255)  null,
    nickname      varchar(255)  null,
    summery       varchar(1000) null,
    user_id       bigint        null,
    constraint UK_3dc88siimqcmjpn8mwr8tmr9m
        unique (user_id),
    constraint UK_9abgwqmvs2sd7o1p3g43xj0uq
        unique (nickname),
    constraint UK_qphtmijovoh2703hb7hvvpoy9
        unique (mobile_number),
    constraint UK_tvrrlr6kh8f4ljld8llhvkt4
        unique (image_path),
    constraint FKlltiuwkifce5ms2rgf3yjfv6k
        foreign key (user_id) references users (id)
            on delete cascade
);

create index user_bio_id_IDX
    on user_bio (id);

create index users_email_IDX
    on users (email);

create index users_full_name_IDX
    on users (full_name);

create index users_id_IDX
    on users (id);


