package com.egsians.model;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import static org.hibernate.annotations.OnDeleteAction.CASCADE;

@Data
@Entity
@Table(name = "post_comments")
public class PostCommentEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @OneToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @OneToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "post_id")
    @OnDelete(action = CASCADE)
    private PostEntity post;

    @Column(name = "post_comment", length = 1000)
    private String postComment;

    @OneToMany(fetch = FetchType.LAZY,
            cascade = {CascadeType.REMOVE, CascadeType.PERSIST})
    private List<PostCommentEntity> replies;


    @OnDelete(action = CASCADE)
    @ManyToOne(fetch = FetchType.LAZY)
    private PostCommentEntity parent;
    @CreationTimestamp
    private Date time;

}
