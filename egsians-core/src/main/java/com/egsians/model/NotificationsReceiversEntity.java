package com.egsians.model;

import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Data
@Entity
@Table(name = "notifications_receivers", indexes = {
        @Index(name = "notifications_id_user_id_seen_IDX", columnList = "notifications_id,user_id,seen", unique = true)
})
public class NotificationsReceiversEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "notifications_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private NotificationEntity notificationEntity;

    @ManyToOne
    @JoinColumn(name = "user_id", updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private UserEntity userEntity;

    @Column(name = "seen")
    private Boolean isSeen = false;

}
