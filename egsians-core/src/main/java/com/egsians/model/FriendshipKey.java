package com.egsians.model;

import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.io.Serializable;

import static org.hibernate.annotations.OnDeleteAction.CASCADE;

@Data
@Embeddable
public class FriendshipKey implements Serializable {

    @OneToOne
    @JoinColumn(name = "user_id")
    @OnDelete(action = CASCADE)
    private UserEntity user;

    @OneToOne
    @JoinColumn(name = "friend_id")
    @OnDelete(action = CASCADE)
    private UserEntity friend;

}
