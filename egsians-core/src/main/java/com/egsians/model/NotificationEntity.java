package com.egsians.model;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "notifications",indexes = {
        @Index(name = "notifications_user_id_IDX",columnList = "user_id")
})
public class NotificationEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private UserEntity userEntity;

    @Column(name = "text", length = 1000, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private String text;

    @CreationTimestamp
    @Column(name = "date",updatable = false)
    private Date date;

}
