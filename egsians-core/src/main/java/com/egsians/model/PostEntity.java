package com.egsians.model;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import static org.hibernate.annotations.OnDeleteAction.CASCADE;

@Data
@Entity
@Table(name = "posts", indexes = {
        @Index(name = "posts_id_IDX", columnList = "id"),
        @Index(name = "posts_user_id_IDX", columnList = "user_id")
})
public class PostEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    @OnDelete(action = CASCADE)
    private UserEntity user;

    @Column(name = "text", nullable = false, length = 1000)
    private String text;

    @CreationTimestamp
    @Column(updatable = false, name = "date")
    private Date date;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "post_id")
    private List<ImageEntity> images;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(name = "hash_tags_posts",
            joinColumns = @JoinColumn(name = "post_id"),
            inverseJoinColumns = @JoinColumn(name = "hash_tag_id")
    )
    private List<HashTagEntity> hashTags;
}
