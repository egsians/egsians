package com.egsians.model;

import com.egsians.util.FriendshipStatusEnum;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "friendship", indexes = {
        @Index(name = "friendship_user_id_IDX", columnList = "user_id"),
        @Index(name = "friendship_friend_id_IDX", columnList = "friend_id"),
        @Index(name = "friendship_user_friend_id_IDX", columnList = "user_id,friend_id", unique = true)
})
public class FriendshipEntity {

    @EmbeddedId
    private FriendshipKey friendshipId;

    @Enumerated(EnumType.STRING)
    private FriendshipStatusEnum status;
}
