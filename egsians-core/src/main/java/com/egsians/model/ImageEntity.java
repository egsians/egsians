package com.egsians.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "images", indexes = {
        @Index(name = "images_id_IDX", columnList = "id")
})
public class ImageEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    private PostEntity post;

    @Column(name = "image_path", unique = true, nullable = false)
    private String imagePath;

}
