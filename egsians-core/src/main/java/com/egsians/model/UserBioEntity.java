package com.egsians.model;

import lombok.Data;
import org.hibernate.annotations.OnDelete;

import javax.persistence.*;
import java.util.Date;

import static org.hibernate.annotations.OnDeleteAction.CASCADE;

@Data
@Entity
@Table(name = "user_bio", indexes = {
        @Index(name = "user_bio_id_IDX", columnList = "id")}
)
public class UserBioEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id",unique = true)
    @OnDelete(action = CASCADE)
    private UserEntity user;

    @Column(name = "birthday")
    private Date birthday;

    @Column(name = "summery", length = 1000)
    private String summery;

    @Column(name = "mobile_number", unique = true)
    private String mobileNumber;

    @Column(name = "nickname", unique = true)
    private String nickname;

    @Column(name = "image_path", unique = true)
    private String imagePath;

}
