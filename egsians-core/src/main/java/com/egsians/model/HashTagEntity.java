package com.egsians.model;

import lombok.Data;
import org.hibernate.annotations.OnDelete;

import javax.persistence.*;
import java.util.List;

import static org.hibernate.annotations.OnDeleteAction.CASCADE;

@Data
@Entity
@Table(name = "hash_tags")
public class HashTagEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, insertable = false)
    private Long id;

    @Column(name = "text", updatable = false, unique = true)
    private String text;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @OnDelete(action = CASCADE)
    @JoinTable(name = "hash_tags_posts",
            joinColumns = @JoinColumn(name = "hash_tag_id"),
            inverseJoinColumns = @JoinColumn(name = "post_id")
    )
    private List<PostEntity> posts;
}
