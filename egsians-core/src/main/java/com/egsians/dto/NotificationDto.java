package com.egsians.dto;

import lombok.Data;

import java.util.Date;

@Data
public class NotificationDto {
    private Long id;
    private Long userId;
    private String text;
    private Date date;
}
