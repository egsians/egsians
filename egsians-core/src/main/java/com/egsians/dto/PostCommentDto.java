package com.egsians.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostCommentDto {

    private Long id;
    private Long userId;
    private Long postId;
    private String postComment;
    private List<PostCommentDto> replies;
    private Date time;
}
