package com.egsians.dto;

import lombok.Data;

@Data
public class HashTagDto {
    private Long id;
    private String text;
}
