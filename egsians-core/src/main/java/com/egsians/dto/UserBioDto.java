package com.egsians.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserBioDto {

    private Long id;
    private Long userId;
    private Date birthday;
    private String summery;
    private String mobileNumber;
    private String nickname;
    private String imagePath;
}
