package com.egsians.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FriendshipDto {

    private Long userId;
    private Long friendId;
    private String status;

}
