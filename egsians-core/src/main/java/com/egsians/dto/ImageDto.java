package com.egsians.dto;

import lombok.Data;

@Data
public class ImageDto {
    private Long id;
    private Long postId;
    private String imagePath;
}
