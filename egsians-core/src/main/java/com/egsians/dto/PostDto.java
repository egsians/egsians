package com.egsians.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostDto {

    private Long id;
    private Long userId;
    private String text;
    private Date date;
    private List<HashTagDto> hashTags;
    private List<ImageDto> images;

}
