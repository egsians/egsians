package com.egsians.mapper;

import com.egsians.dto.UserBioDto;
import com.egsians.model.UserBioEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserBioMapperTest {
    @Autowired
    private UserBioMapper bioMapper;


    @Test
    public void test_UserBioEntityToUserBioDto_Success() {
        UserBioDto bioDto = new UserBioDto();
        bioDto.setBirthday(new Date());
        bioDto.setMobileNumber("+126541");
        bioDto.setUserId(2L);
        bioDto.setNickname("name");
        UserBioEntity mappedBioEntity = bioMapper.map(bioDto, UserBioEntity.class);
        assertEquals(bioDto.getBirthday(), mappedBioEntity.getBirthday());
        assertEquals(bioDto.getId(), mappedBioEntity.getId());
        assertEquals(bioDto.getUserId(), mappedBioEntity.getUser().getId());
        assertEquals(bioDto.getNickname(), mappedBioEntity.getNickname());
    }

}