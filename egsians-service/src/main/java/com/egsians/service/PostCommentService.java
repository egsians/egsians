package com.egsians.service;

import com.egsians.dto.PostCommentDto;
import com.egsians.dto.PostDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface PostCommentService {

    Long getCountOfCommentsForPost(Long postId);

    Page<PostCommentDto> getCommentsOfPost(Long postId, Pageable pageable);

    PostCommentDto addCommentToPost(PostCommentDto comment);

    void replyToComment(Long parentId, PostCommentDto child);

    void deleteComment(PostCommentDto comment);

    Long getOwnerId(PostCommentDto postCommentDto);

    Page<PostCommentDto> getReplies(Long commentId, Pageable pageable);
}
