package com.egsians.service;

import com.egsians.dto.NotificationDto;
import com.egsians.dto.PostCommentDto;
import com.egsians.dto.PostDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface NotificationService {

    void addNotification(Long userId, PostCommentDto postCommentDto);

    void addNotification(Long userId, PostDto postDto);

    void sendEmail(Long userId);

    Page<NotificationDto> getNotificationsForUser(Long userId, Pageable pageable);

    void setSeen(Long userId, Long notificationId);

    Page<NotificationDto> getNotificationsFromConcreteUser(Long notificationCreatorId, Pageable pageable);
}
