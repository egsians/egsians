package com.egsians.service;

import com.egsians.dto.HashTagDto;
import com.egsians.dto.PostDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

public interface PostService {
    void addPost(PostDto post);

    void updatePost(PostDto post);

    void deletePost(Long postId);


    PostDto findPostById(Long id);

    Page<PostDto> findAllPostsByUser(Long userId, Pageable pageable);

    Page<PostDto> findAllByHashTag(String hashTag, Pageable pageable);

    Long getOwnerId(PostDto postDto);
}
