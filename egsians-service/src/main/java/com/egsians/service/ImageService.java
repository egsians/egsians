package com.egsians.service;

import com.egsians.dto.ImageDto;
import com.egsians.dto.PostDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface ImageService {

    ImageDto addForPost(MultipartFile iamge, PostDto post) throws IOException;

    String addForUser(MultipartFile image, Long userId) throws IOException;

    public Page<ImageDto> getByPost(PostDto post, Pageable pageable);

    void delete(ImageDto picture) throws IOException;

    byte[] getPictureBytes(String imagePath) throws IOException;

}
