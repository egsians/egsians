package com.egsians.service;

import com.egsians.dto.UserBioDto;
import com.egsians.dto.UserDto;
import com.egsians.security.jwt.JwtUser;
import com.egsians.util.exception.EmptyFriendListException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;

public interface UserService {
    UserDto save(UserDto user);

    UserDto update(UserDto user);

    UserBioDto saveBio(UserBioDto user, MultipartFile image) throws IOException;

    void remove(Long userId);

    UserDto findById(Long userId);

    UserBioDto getBio(Long userId);

    Page<UserDto> findByTextPattern(String text, Pageable pageable, Date from, Date to);

    Page<UserDto> findByNameAndAge(String name, Date from, Date to, Pageable pageable);

    UserDto findByEmail(String email);

    UserDto findByPhoneNumber(String mobileNumber);

    JwtUser getCurrentUser();

    boolean isUserAuthorized(Long requestSenderId);

    Boolean isFriends(Long userId, Long friendId);
}
