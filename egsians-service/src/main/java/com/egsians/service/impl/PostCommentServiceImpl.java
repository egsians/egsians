package com.egsians.service.impl;

import com.egsians.dto.PostCommentDto;
import com.egsians.mapper.PostCommentMapper;
import com.egsians.mapper.PostMapper;
import com.egsians.model.PostCommentEntity;
import com.egsians.model.PostEntity;
import com.egsians.model.UserEntity;
import com.egsians.repository.PostCommentRepository;
import com.egsians.repository.PostRepository;
import com.egsians.service.NotificationService;
import com.egsians.service.PostCommentService;
import com.egsians.service.UserService;
import com.egsians.util.exception.PostCommentNotFoundException;
import com.egsians.util.exception.PostNotFoundException;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
public class PostCommentServiceImpl implements PostCommentService {
    private final PostCommentRepository commentRepo;
    private final PostMapper postMapper;
    private final PostCommentMapper commentMapper;
    private final PostRepository postRepository;
    private final UserService userService;
    private final NotificationService notificationService;

    public PostCommentServiceImpl(PostCommentRepository commentRepo, PostMapper postMapper,
                                  PostCommentMapper commentMapper, PostRepository postRepository,
                                  @Lazy UserService userService, NotificationService notificationService) {
        this.commentRepo = commentRepo;
        this.postMapper = postMapper;
        this.commentMapper = commentMapper;
        this.postRepository = postRepository;
        this.userService = userService;
        this.notificationService = notificationService;
    }


    @Override
    public Long getCountOfCommentsForPost(Long postId) {
        PostEntity postEntity = new PostEntity();
        postEntity.setId(postId);
        PostCommentEntity postCommentEntity = new PostCommentEntity();
        postCommentEntity.setPost(postEntity);
        return commentRepo.count(Example.of(postCommentEntity));
    }

    @Override
    public Page<PostCommentDto> getCommentsOfPost(Long postId, Pageable pageable) {
        PostEntity postEntity = postRepository.findById(postId).
                orElseThrow(() -> new PostNotFoundException(postId.toString()));
        Page<PostCommentEntity> allByPost = commentRepo.findAllByPost(postEntity, pageable);
        return allByPost.map(comment -> commentMapper.map(comment, PostCommentDto.class));
    }

    @Override
    @Transactional
    public PostCommentDto addCommentToPost(PostCommentDto comment) {
        PostEntity postEntity = postRepository.findById(comment.getPostId()).
                orElseThrow(() -> new PostNotFoundException(comment.getPostId().toString()));
        if (userService.isFriends(postEntity.getId(), comment.getUserId())) {
            PostCommentEntity commentEntity = commentMapper.map(comment, PostCommentEntity.class);
            commentEntity.setPost(postEntity);
            commentEntity = commentRepo.save(commentEntity);
            commentMapper.map(commentEntity, comment);
            notificationService.addNotification(comment.getUserId(), comment);
            return comment;


        }
        throw new IllegalArgumentException("User with id: " + comment.getUserId() +
                " can't write comment for this post.");
    }

    @Override
    @Transactional
    public void replyToComment(Long parentId, PostCommentDto childPostCommentEntity) {
        PostCommentEntity parentCommentEntity = commentRepo.findById(parentId)
                .orElseThrow(() -> new PostCommentNotFoundException(parentId.toString()));
        PostCommentEntity childCommentEntity = commentMapper.map(childPostCommentEntity, PostCommentEntity.class);
        childCommentEntity.setId(null);
        parentCommentEntity.getReplies().add(childCommentEntity);
        childCommentEntity.setParent(parentCommentEntity);
        commentRepo.save(parentCommentEntity);
        commentRepo.save(childCommentEntity);
        notificationService.addNotification(childPostCommentEntity.getUserId(), childPostCommentEntity);

    }

    @Override
    @Transactional
    public void deleteComment(PostCommentDto postCommentDto) {
        PostCommentEntity postCommentEntity = commentMapper.map(postCommentDto, PostCommentEntity.class);
        commentRepo.delete(postCommentEntity);

    }

    @Override
    public Long getOwnerId(PostCommentDto postCommentDto) {
        Optional<PostCommentEntity> optionalPostCommentEntity = commentRepo.findById(postCommentDto.getId());
        return optionalPostCommentEntity
                .map(PostCommentEntity::getUser)
                .map(UserEntity::getId)
                .orElseThrow(() -> new PostCommentNotFoundException(postCommentDto.getId().toString()));
    }

    @Override
    public Page<PostCommentDto> getReplies(Long commentId, Pageable pageable) {
        PostCommentEntity postCommentEntity = commentRepo.findById(commentId)
                .orElseThrow(() -> new PostCommentNotFoundException(commentId.toString()));

        List<PostCommentDto> postCommentDtos = postCommentEntity.getReplies()
                .stream().map(commentEntity -> commentMapper
                        .map(commentEntity, PostCommentDto.class))
                .collect(Collectors.toList());

        return new PageImpl<>(postCommentDtos, pageable, postCommentDtos.size());
    }
}

