package com.egsians.service.impl;

import com.egsians.dto.PostDto;
import com.egsians.mapper.PostMapper;
import com.egsians.model.HashTagEntity;
import com.egsians.model.PostEntity;
import com.egsians.repository.HashTagRepository;
import com.egsians.service.HashTagService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
public class HashTagServiceImpl implements HashTagService {
    private final HashTagRepository hashTagRepository;

    private final PostMapper postMapper;

    public HashTagServiceImpl(HashTagRepository hashTagRepository, PostMapper postMapper) {
        this.hashTagRepository = hashTagRepository;
        this.postMapper = postMapper;
    }

    @Override
    @Transactional
    public void addHashTags(PostDto postDto) {
        PostEntity postEntity = postMapper.map(postDto, PostEntity.class);
        List<HashTagEntity> tagEntities = hashTagRepository.findAll();

        postEntity.getHashTags().forEach(hashTag -> {
            AtomicBoolean hashTagExist = new AtomicBoolean(false);
            tagEntities.forEach(hashTagEntity -> {
                if (hashTagEntity.getText().equals(hashTag.getText())) {
                    hashTagEntity.getPosts().add(postEntity);
                    hashTagRepository.save(hashTagEntity);
                    hashTagExist.set(true);
                }
            });

            if (!hashTagExist.get()) {
                hashTagRepository.save(hashTag);
            }
        });
    }

    @Transactional
    public void addHashTag(PostDto postDto) {
        PostEntity postEntity = postMapper.map(postDto, PostEntity.class);
        List<String> allTagNames = hashTagRepository
                .findAll().stream()
                .map(HashTagEntity::getText).collect(Collectors.toList());
        postDto.getHashTags().forEach(hashTagDto -> {
            if (!allTagNames.contains(hashTagDto.getText())) {
                HashTagEntity hashTagEntity = new HashTagEntity();
                hashTagEntity.setText(hashTagDto.getText());
                hashTagEntity.getPosts().add(postEntity);
                hashTagRepository.save(hashTagEntity);
            } else {
                hashTagRepository.findByText(hashTagDto.getText())
                        .ifPresent(hashTagEntity -> hashTagEntity.getPosts().add(postEntity));
            }
        });
    }
}
