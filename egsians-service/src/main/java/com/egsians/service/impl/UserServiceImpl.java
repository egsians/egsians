package com.egsians.service.impl;

import com.egsians.dto.PostDto;
import com.egsians.dto.UserBioDto;
import com.egsians.dto.UserDto;
import com.egsians.mapper.UserBioMapper;
import com.egsians.mapper.UserMapper;
import com.egsians.model.UserBioEntity;
import com.egsians.model.UserEntity;
import com.egsians.repository.UserBioRepository;
import com.egsians.repository.UserRepository;
import com.egsians.security.jwt.JwtUser;
import com.egsians.service.*;
import com.egsians.util.exception.UserNotFoundException;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;
import java.util.regex.Pattern;

@Service
@Transactional(readOnly = true)
public class UserServiceImpl implements UserService {
    private static final String EMAIL_PATTERN = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
    private static final String MOBILE_NUMBER_PATTERN = "^\\+(?:[0-9] ?){6,14}[0-9]$";
    private static final String FULL_NAME_PATTERN = "^([a-zA-Z\\s]*[a-zA-Z]+\\s{1}[a-zA-Z]+)|[a-zA-Z]*$";

    private final Pattern emailPattern = Pattern.compile(EMAIL_PATTERN);
    private final Pattern mobileNumberPattern = Pattern.compile(MOBILE_NUMBER_PATTERN);
    private final Pattern fullNamePattern = Pattern.compile(FULL_NAME_PATTERN);

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final UserBioMapper bioMapper;
    private final UserBioRepository userBioRepository;
    private final FriendshipService friendshipService;
    private final PostService postService;
    private final PasswordEncoder passwordEncoder;
    private final ImageService imageService;
    private final NotificationService notificationService;


    public UserServiceImpl(UserRepository userRepository, UserMapper userMapper,
                           UserBioMapper bioMapper, UserBioRepository userBioRepository,
                           @Lazy FriendshipService friendshipService, PostService postService,
                           PasswordEncoder passwordEncoder, ImageService imageService,
                           @Lazy NotificationService notificationService) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
        this.bioMapper = bioMapper;
        this.userBioRepository = userBioRepository;
        this.friendshipService = friendshipService;
        this.postService = postService;
        this.passwordEncoder = passwordEncoder;
        this.imageService = imageService;
        this.notificationService = notificationService;
    }

    @Override
    @Transactional
    public UserDto save(UserDto userDto) {
        UserEntity userEntity = userMapper.map(userDto, UserEntity.class);
        userEntity.setId(null);
        userEntity.setPassword(passwordEncoder.encode(userEntity.getPassword()));
        try {
            userEntity = userRepository.save(userEntity);
        } catch (DataIntegrityViolationException ex) {
            throw new IllegalArgumentException("User with the same email already exists.");
        }

        userMapper.map(userEntity, userDto);
        notificationService.sendEmail(userEntity.getId());
        return userDto;
    }

    @Override
    @Transactional
    public UserDto update(UserDto userDto) {
        if (userDto == null || userDto.getId() == null) {
            throw new IllegalArgumentException("User id should not be null.");
        }
        Optional<UserEntity> optionalUserEntity = userRepository.findById(userDto.getId());
        UserEntity userEntity = optionalUserEntity.orElseThrow(
                () -> new UserNotFoundException("User with id " + userDto.getId() + " not found."));
        if (userDto.getEmail() != null && !userDto.getEmail().equals(userEntity.getEmail())) {
            throw new RuntimeException("User can't change email address.");
        }
        userMapper.map(userDto, userEntity);
        userEntity = userRepository.save(userEntity);
        userMapper.map(userEntity, userDto);
        return userDto;
    }

    @Override
    @Transactional
    public UserBioDto saveBio(UserBioDto userBioDto, MultipartFile image) throws IOException {
        if (userBioDto.getUserId() == null) {
            throw new IllegalArgumentException();
        }
        UserBioEntity mappedBioEntity = bioMapper.map(userBioDto, UserBioEntity.class);
        if (image != null) {
            String imagePath = imageService.addForUser(image, userBioDto.getUserId());
            userBioDto.setImagePath(imagePath);
        }
        userBioRepository.save(mappedBioEntity);
        bioMapper.map(mappedBioEntity, userBioDto);
        return userBioDto;
    }

    @Override
    @Transactional
    public void remove(Long userId) {
        Optional<UserEntity> optionalUserEntity = userRepository.findById(userId);

        UserEntity userEntity = optionalUserEntity.
                orElseThrow(() -> new UserNotFoundException(
                        "User with id " + userId + " not found"));
        Optional<UserBioEntity> optionalUserBioEntity = userBioRepository.findByUser(userEntity);
        optionalUserBioEntity.ifPresent(userBioRepository::delete);
        friendshipService.deleteAllFriendshipRelations(userId);
        Page<PostDto> allPostsByUser = postService.findAllPostsByUser(userId, Pageable.unpaged());
        allPostsByUser.stream().map(PostDto::getId).forEach(postService::deletePost);
        userRepository.delete(userEntity);
    }

    @Override
    public UserDto findById(Long userId) {
        Optional<UserEntity> optionalUserEntity = userRepository.findById(userId);
        UserEntity userEntity = optionalUserEntity.
                orElseThrow(() -> new UserNotFoundException("User with id " + userId + " not found"));
        return userMapper.map(userEntity, UserDto.class);

    }


    @Override
    public UserBioDto getBio(Long userId) {
        UserEntity userEntity = userRepository.findById(userId).
                orElseThrow(() -> new UserNotFoundException("User with id " + userId + " not found"));
        Optional<UserBioEntity> optionalBioEntity = userBioRepository.findByUser(userEntity);
        UserBioEntity userBioEntity = optionalBioEntity.orElseThrow(
                () -> new UserNotFoundException("User with id " + userId + " not found."));
        return bioMapper.map(userBioEntity, UserBioDto.class);
    }

    @Override
    public Page<UserDto> findByTextPattern(String text, Pageable pageable, Date from, Date to) {
        if (fullNamePattern.matcher(text).matches()) {
            return findByNameAndAge(text, from, to, pageable);
        }
        if (emailPattern.matcher(text).matches()) {
            UserDto byEmail = findByEmail(text);
            return new PageImpl<>(Collections.singletonList(byEmail), pageable, 1);
        }
        if (mobileNumberPattern.matcher(text).matches()) {
            UserDto userDto = findByPhoneNumber(text);
            return new PageImpl<>(Collections.singletonList(userDto), pageable, 1);
        }
        throw new IllegalArgumentException("Invalid search text: " + text);
    }


    @Override
    public Page<UserDto> findByNameAndAge(String fullName, Date from, Date to, Pageable pageable) {
        String[] names = fullName.split(" ");
        Page<UserEntity> userEntities = userRepository.findAllByNameAndAge(names, from, to, pageable);
        return userEntities.map(userEntity -> userMapper.map(userEntity, UserDto.class));
    }

    @Override
    public UserDto findByEmail(String email) {
        if (StringUtils.isEmpty(email)) {
            throw new IllegalArgumentException("User's email should not be empty");
        }
        Optional<UserEntity> optionalUser = userRepository.findByEmailIgnoreCase(email);
        UserEntity userEntity = optionalUser.orElseThrow(
                () -> new UserNotFoundException("User with email " + email + " not found"));
        return userMapper.map(userEntity, UserDto.class);
    }

    @Override
    public UserDto findByPhoneNumber(String mobileNumber) {
        if (StringUtils.isEmpty(mobileNumber)) {
            throw new IllegalArgumentException("User's mobile number should not be empty");
        }
        Optional<UserBioEntity> optionalBioEntity = userBioRepository.findByMobileNumber(mobileNumber);
        UserBioEntity bioEntity = optionalBioEntity.orElseThrow(
                () -> new UserNotFoundException("User with mobile number "
                        + mobileNumber + " not found."));
        return userMapper.map(bioEntity.getUser(), UserDto.class);
    }

    @Override
    public boolean isUserAuthorized(Long requestSenderId) {
        return getCurrentUser().getId().equals(requestSenderId);
    }

    @Override
    public JwtUser getCurrentUser() {
        return (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    @Override
    public Boolean isFriends(Long userId, Long friendId) {
        Page<UserDto> friends = friendshipService.getFriendsList(userId, Pageable.unpaged());
        return friends.stream().anyMatch(userDto -> userDto.getId().equals(friendId));
    }
}


