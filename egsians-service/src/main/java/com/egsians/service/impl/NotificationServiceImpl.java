package com.egsians.service.impl;

import com.egsians.dto.NotificationDto;
import com.egsians.dto.PostCommentDto;
import com.egsians.dto.PostDto;
import com.egsians.dto.UserDto;
import com.egsians.mapper.NotificationMapper;
import com.egsians.model.NotificationEntity;
import com.egsians.model.NotificationsReceiversEntity;
import com.egsians.model.UserEntity;
import com.egsians.repository.NotificationRepository;
import com.egsians.repository.NotificationsReceiversRepository;
import com.egsians.repository.UserRepository;
import com.egsians.service.FriendshipService;
import com.egsians.service.NotificationService;
import com.egsians.service.UserService;
import com.egsians.util.exception.NotificationNotFoundException;
import com.egsians.util.exception.UserNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;

/**
 * @author Astghik Melkumyan, Arman Arshakyan
 */
@Service
@Transactional(readOnly = true)
public class NotificationServiceImpl implements NotificationService {
    private FriendshipService friendshipService;
    private UserRepository userRepository;
    private NotificationRepository notificationRepository;
    private NotificationMapper notificationMapper;
    private NotificationsReceiversRepository notificationsReceiversRepository;
    private JavaMailSender javaMailSender;
    private UserService userService;


    public NotificationServiceImpl(UserRepository userRepository, NotificationRepository notificationRepository,
                                   NotificationMapper notificationMapper,
                                   NotificationsReceiversRepository notificationsReceiversRepository,
                                   JavaMailSender javaMailSender, UserService userService,
                                   FriendshipService friendshipService) {
        this.userRepository = userRepository;
        this.notificationRepository = notificationRepository;
        this.notificationMapper = notificationMapper;
        this.notificationsReceiversRepository = notificationsReceiversRepository;
        this.javaMailSender = javaMailSender;
        this.userService = userService;
        this.friendshipService = friendshipService;
    }

    @Override
    @Transactional
    public void addNotification(Long userId, PostCommentDto postCommentDto) {
        UserEntity userEntity = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException(userId.toString()));
        UserDto postAuthor = userService.findById(postCommentDto.getUserId());
        NotificationEntity notificationEntity = new NotificationEntity();
        notificationEntity.setUserEntity(userEntity);
        StringBuilder stringBuilder = new StringBuilder()
                .append(userEntity.getFirstName()).append(" ").append(userEntity.getLastName())
                .append(" commented to post of ")
                .append(postAuthor.getFirstName()).append(" ").append(postAuthor.getLastName())
                .append(". ").append(postCommentDto.getPostComment(), 0, 5).append("...");
        notificationEntity.setText(stringBuilder.toString());
        notificationRepository.save(notificationEntity);
        addFriendsToReceiversList(userEntity, notificationEntity);
    }

    @Transactional
    public void addFriendsToReceiversList(UserEntity userEntity, NotificationEntity notificationEntity) {
        Page<UserDto> friendsList = friendshipService
                .getFriendsList(userService.getCurrentUser().getId(), Pageable.unpaged());
        friendsList.forEach(userDto -> {
            NotificationsReceiversEntity notificationsReceiversEntity = new NotificationsReceiversEntity();
            notificationsReceiversEntity.setNotificationEntity(notificationEntity);
            UserEntity friend = userRepository.findById(userDto.getId())
                    .orElseThrow(() -> new UserNotFoundException(userDto.getId().toString()));
            notificationsReceiversEntity.setUserEntity(friend);
            notificationsReceiversRepository.saveAndFlush(notificationsReceiversEntity);
        });
    }

    @Override
    @Transactional
    public void addNotification(Long userId, PostDto postDto) {
        UserEntity userEntity = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException(userId.toString()));
        NotificationEntity notificationEntity = new NotificationEntity();
        notificationEntity.setUserEntity(userEntity);
        String notificationText = new StringBuilder()
                .append(userEntity.getFirstName()).append(" ").append(userEntity.getLastName())
                .append(" created the post. ").append(postDto.getText(), 0, 5).append("...")
                .toString();
        notificationEntity.setText(notificationText);
        notificationRepository.save(notificationEntity);
        addFriendsToReceiversList(userEntity, notificationEntity);
    }

    @Override
    public void sendEmail(Long userId) {
        UserEntity userEntity = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException(userId.toString()));
        String email = userEntity.getEmail();
        //chgitem mnnacace inch es uzum anes
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo(email);
        simpleMailMessage.setSubject("Notification from EGSians");
        String mailMessage = new StringBuilder("Dear ")
                .append(userEntity.getFirstName())
                .append(" ").append(userEntity.getLastName())
                .append(".")
                .append("\n Thank you for registration in Egsians.")
                .append("\n Best regards.")
                .append("\n EGSians team.").toString();
        simpleMailMessage.setText(mailMessage);
        javaMailSender.send(simpleMailMessage);
        //senc mi ban  ei qeznic spasum :)
    }

    @Override
    public Page<NotificationDto> getNotificationsFromConcreteUser(
            Long notificationCreatorId, Pageable pageable) {
        if (userService.isFriends(userService.getCurrentUser().getId(), notificationCreatorId)) {
            Page<NotificationsReceiversEntity> notifications = notificationsReceiversRepository
                    .findAllById(notificationCreatorId, pageable);
            return notifications.map(notificationsReceiversEntity -> notificationMapper
                    .map(notificationsReceiversEntity.getNotificationEntity(), NotificationDto.class));
        }
        return new PageImpl<>(Collections.emptyList());
    }

    @Override
    @Transactional
    public void setSeen(Long userId, Long notificationId) {
        UserEntity userEntity = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId.toString()));
        NotificationEntity notificationEntity = notificationRepository.findById(notificationId)
                .orElseThrow(() -> new NotificationNotFoundException(notificationId.toString()));
        NotificationsReceiversEntity notificationsReceiversEntity = notificationsReceiversRepository
                .findByUserEntityAndNotificationEntity(userEntity, notificationEntity)
                .orElseThrow(() -> new NotificationNotFoundException(notificationEntity.toString()));
        notificationsReceiversEntity.setIsSeen(true);
        notificationsReceiversRepository.save(notificationsReceiversEntity);
    }

    @Override
    public Page<NotificationDto> getNotificationsForUser(Long userId, Pageable pageable) {
        UserEntity userEntity = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException(userId.toString()));
        Page<NotificationsReceiversEntity> allByUserEntityAndIsSeen = notificationsReceiversRepository
                .findAllByUserEntityAndIsSeen(userEntity, Boolean.FALSE, pageable);
        return allByUserEntityAndIsSeen.map(notificationsReceiversEntity ->
                notificationMapper.map(notificationsReceiversEntity.getNotificationEntity(),
                        NotificationDto.class)
        );
    }
}
