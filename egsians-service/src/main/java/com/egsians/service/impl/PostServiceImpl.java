package com.egsians.service.impl;

import com.egsians.dto.ImageDto;
import com.egsians.dto.PostCommentDto;
import com.egsians.dto.PostDto;
import com.egsians.mapper.ImageMapper;
import com.egsians.mapper.PostMapper;
import com.egsians.model.HashTagEntity;
import com.egsians.model.PostEntity;
import com.egsians.model.UserEntity;
import com.egsians.repository.HashTagRepository;
import com.egsians.repository.PostRepository;
import com.egsians.service.*;
import com.egsians.util.exception.PostNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Collections;
import java.util.Optional;


@Service
@Transactional(readOnly = true)
public class PostServiceImpl implements PostService {

    private final PostRepository postRepository;
    private final PostMapper postMapper;
    private final ImageService imageService;
    private final ImageMapper imageMapper;
    private final PostCommentService commentService;
    private final HashTagService hashTagService;
    private final NotificationService notificationService;
    private final HashTagRepository hashTagRepository;


    public PostServiceImpl(PostRepository postRepository, PostMapper postMapper,
                           ImageService imageService, ImageMapper imageMapper,
                           PostCommentService commentService,
                           HashTagService hashTagService, NotificationService notificationService, HashTagRepository hashTagRepository) {
        this.postRepository = postRepository;
        this.postMapper = postMapper;
        this.imageService = imageService;
        this.imageMapper = imageMapper;
        this.commentService = commentService;
        this.hashTagService = hashTagService;
        this.notificationService = notificationService;
        this.hashTagRepository = hashTagRepository;
    }


    @Override
    @Transactional
    public void addPost(PostDto postDto) {
        PostEntity postEntity = postMapper.map(postDto, PostEntity.class);
        postEntity.setId(null);
        postRepository.save(postEntity);
        notificationService.addNotification(postDto.getUserId(), postDto);
    }

    @Override
    @Transactional
    public void updatePost(PostDto postDto) {
        if (postDto.getId() == null) {
            throw new IllegalArgumentException("Post id should not be null!");
        }
        PostEntity postEntity = postMapper.map(postDto, PostEntity.class);
        postEntity = postRepository.save(postEntity);
        postMapper.map(postEntity, postDto);
    }

    @Override
    @Transactional
    public void deletePost(Long postId) {
        if (postId == null) {
            throw new IllegalArgumentException("Post id should not be null!");
        }

        Optional<PostEntity> optionalPostEntity = postRepository.findById(postId);
        PostEntity postEntity = optionalPostEntity
                .orElseThrow(() -> new PostNotFoundException("Post with id" + postId + "not found"));
        postEntity.getImages().forEach(image -> {
            ImageDto imageDto = imageMapper.map(image, ImageDto.class);
            try {
                imageService.delete(imageDto);
            } catch (IOException ignored) {
            }
        });
        Page<PostCommentDto> commentsOfPost = commentService.getCommentsOfPost(postId, Pageable.unpaged());
        commentsOfPost.forEach(commentService::deleteComment);
        postRepository.delete(postEntity);
    }


    @Override
    public PostDto findPostById(Long id) {
        Optional<PostEntity> optionalPostEntity = postRepository.findById(id);
        PostEntity postEntity = optionalPostEntity.
                orElseThrow(() -> new PostNotFoundException("Post with id " + id + " not found"));
        return postMapper.map(postEntity, PostDto.class);
    }

    @Override
    public Page<PostDto> findAllPostsByUser(Long userId, Pageable pageable) {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(userId);
        Page<PostEntity> postEntities = postRepository.findAllByUser(userEntity, pageable);
        return postEntities.map(postEntity -> postMapper.map(postEntity, PostDto.class));
    }

    @Override
    public Page<PostDto> findAllByHashTag(String hashTag, Pageable pageable) {
        Optional<HashTagEntity> optionalHashTagEntity = hashTagRepository.findByText(hashTag);
        if (optionalHashTagEntity.isPresent()) {
            Page<PostEntity> postEntities = postRepository.findAllByHashTags(optionalHashTagEntity.get(), pageable);
            return postEntities.map(postEntity -> postMapper.map(postEntity, PostDto.class));
        }
        return new PageImpl<>(Collections.emptyList(), pageable, 0);
    }

    @Override
    public Long getOwnerId(PostDto postDto) {
        Optional<PostEntity> optionalPostEntity = postRepository.findById(postDto.getId());
        return optionalPostEntity
                .map(PostEntity::getUser)
                .map(UserEntity::getId)
                .orElseThrow(() -> new PostNotFoundException(postDto.getId().toString()));
    }
}
