package com.egsians.service.impl;

import com.egsians.dto.ImageDto;
import com.egsians.dto.PostDto;
import com.egsians.mapper.ImageMapper;
import com.egsians.mapper.PostMapper;
import com.egsians.model.ImageEntity;
import com.egsians.model.PostEntity;
import com.egsians.repository.ImageRepository;
import com.egsians.service.ImageService;
import org.apache.commons.io.FilenameUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ImageServiceImpl implements ImageService {
    private final ImageRepository imageRepo;
    private final ImageMapper imageMapper;
    private final PostMapper postMapper;

    public ImageServiceImpl(ImageRepository imageRepo, ImageMapper imageMapper, PostMapper postMapper) {
        this.imageRepo = imageRepo;
        this.imageMapper = imageMapper;
        this.postMapper = postMapper;
    }

    @Override
    public ImageDto addForPost(MultipartFile image, PostDto post) throws IOException {
        String fileExtension = FilenameUtils.getExtension(image.getOriginalFilename());
        String fileName = post.getId() + new Date().toString() + "." + fileExtension;
        saveInFileSystem(image, fileName);
        ImageEntity imageEntity = new ImageEntity();
        imageEntity.setImagePath(fileName);
        imageEntity.setPost(postMapper.map(post, PostEntity.class));
        ImageEntity savedImageEntity = imageRepo.save(imageEntity);
        return imageMapper.map(savedImageEntity, ImageDto.class);
    }

    @Override
    public String addForUser(MultipartFile image, Long userId) throws IOException {
        String fileExtension = FilenameUtils.getExtension(image.getOriginalFilename());
        String fileName = userId + new Date().toString() + "." + fileExtension;
        saveInFileSystem(image, fileName);
        return fileName;
    }

    private void saveInFileSystem(MultipartFile image, String name) throws IOException {
        BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(name)));
        stream.write(image.getBytes());
        stream.flush();
        stream.close();
    }

    @Override
    public void delete(ImageDto picture) {
        ImageEntity imageEntity = imageMapper.map(picture, ImageEntity.class);
        String imagePath = imageEntity.getImagePath();
        File file = new File(imagePath);
        boolean isDelete = file.delete();
        if (isDelete) {
            imageRepo.delete(imageEntity);
        }

    }

    @Override
    public Page<ImageDto> getByPost(PostDto post, Pageable pageable) {
        PostEntity postEntity = postMapper.map(post, PostEntity.class);
        List<ImageDto> collected = postEntity.getImages().stream()
                .map(imageEntity -> imageMapper.map(imageEntity, ImageDto.class))
                .collect(Collectors.toList());
        return new PageImpl<>(collected, pageable, collected.size());
    }

    @Override
    public byte[] getPictureBytes(String imagePath) throws IOException {
        File file = new File(imagePath);
        return Files.readAllBytes(file.toPath());
    }

}
