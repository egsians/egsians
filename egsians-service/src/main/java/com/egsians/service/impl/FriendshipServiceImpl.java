package com.egsians.service.impl;

import com.egsians.dto.FriendshipDto;
import com.egsians.dto.UserDto;
import com.egsians.mapper.FriendshipMapper;
import com.egsians.mapper.UserMapper;
import com.egsians.model.FriendshipEntity;
import com.egsians.model.FriendshipKey;
import com.egsians.model.UserEntity;
import com.egsians.repository.FriendshipRepository;
import com.egsians.repository.UserRepository;
import com.egsians.service.FriendshipService;
import com.egsians.service.UserService;
import com.egsians.util.FriendshipStatusEnum;
import com.egsians.util.exception.UserNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.function.Function;

@Service
@Transactional(readOnly = true)
public class FriendshipServiceImpl implements FriendshipService {

    private final FriendshipRepository friendshipRepository;
    private final FriendshipMapper friendshipMapper;
    private final UserService userService;
    private final UserRepository userRepository;

    private final UserMapper userMapper;


    public FriendshipServiceImpl(FriendshipRepository friendshipRepository,
                                 FriendshipMapper friendshipMapper, UserService userService,
                                 UserRepository userRepository, UserMapper userMapper) {
        this.friendshipRepository = friendshipRepository;
        this.friendshipMapper = friendshipMapper;
        this.userService = userService;
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    @Override
    public Page<UserDto> getFriendsList(Long userId, Pageable pageable) {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(userId);
        Page<FriendshipEntity> friendships = friendshipRepository.
                findAllByStatusAndFriendshipId_UserOrFriendshipId_Friend(FriendshipStatusEnum.ACCEPTED,
                        userEntity, userEntity, pageable);

        Function<FriendshipEntity, UserDto> getFriend = friendshipEntity -> {
            UserEntity user = friendshipEntity.getFriendshipId().getUser();
            UserEntity friend = friendshipEntity.getFriendshipId().getFriend();
            if (user.getId().equals(userId)) {
                return userMapper.map(friend, UserDto.class);
            } else {
                return userMapper.map(user, UserDto.class);
            }
        };

        return friendships.map(getFriend);
    }

    @Override
    public Page<UserDto> getFriendshipRequestsToUser(Long userId, Pageable pageable) {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(userId);
        Page<FriendshipEntity> friendships = friendshipRepository.findByStatusAndFriendshipId_Friend(
                FriendshipStatusEnum.WAITING, userEntity, pageable);

        return friendships.map(friendshipEntity ->
                userMapper.map(friendshipEntity.getFriendshipId().getUser(), UserDto.class));
    }

    @Override
    public Page<UserDto> getFriendshipRequestsFromUser(Long userId, Pageable pageable) {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(userId);
        Page<FriendshipEntity> friendships = friendshipRepository.findByStatusAndFriendshipId_User(
                FriendshipStatusEnum.WAITING, userEntity, pageable);

        return friendships.map(friendshipEntity ->
                userMapper.map(friendshipEntity.getFriendshipId().getUser(), UserDto.class));
    }

    @Override
    public Page<UserDto> getRejectedFromUser(Long userId, Pageable pageable) {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(userId);
        Page<FriendshipEntity> friendships = friendshipRepository.findByStatusAndFriendshipId_Friend(
                FriendshipStatusEnum.DECLINED, userEntity, pageable);
        return friendships.map(friendshipEntity ->
                userMapper.map(friendshipEntity.getFriendshipId().getUser(), UserDto.class));
    }

    @Override
    public Page<UserDto> getRejectsToUser(Long userId, Pageable pageable) {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(userId);
        Page<FriendshipEntity> friendships = friendshipRepository.findByStatusAndFriendshipId_User(
                FriendshipStatusEnum.DECLINED, userEntity, pageable);
        return friendships.map(friendshipEntity ->
                userMapper.map(friendshipEntity.getFriendshipId().getUser(), UserDto.class));
    }

    @Override
    @Transactional
    public void sendFriendshipRequest(Long receiverId) {
        FriendshipDto friendshipDto = new FriendshipDto();
        friendshipDto.setUserId(userService.getCurrentUser().getId());
        friendshipDto.setFriendId(receiverId);
        FriendshipEntity friendshipEntity = friendshipMapper.map(friendshipDto, FriendshipEntity.class);
        friendshipRepository.save(friendshipEntity);
    }

    @Override
    @Transactional
    public void acceptInviteFromUser(Long senderId) {
        FriendshipDto friendshipDto = new FriendshipDto();
        friendshipDto.setUserId(senderId);
        friendshipDto.setFriendId(userService.getCurrentUser().getId());
        FriendshipEntity friendshipEntity = friendshipMapper.map(friendshipDto, FriendshipEntity.class);
        friendshipEntity.setStatus(FriendshipStatusEnum.ACCEPTED);
        friendshipRepository.save(friendshipEntity);
    }

    @Override
    @Transactional
    public void declineInviteFromUser(Long senderId) {
        FriendshipDto friendshipDto = new FriendshipDto();
        friendshipDto.setUserId(senderId);
        friendshipDto.setFriendId(userService.getCurrentUser().getId());
        FriendshipEntity friendshipEntity = friendshipMapper.map(friendshipDto, FriendshipEntity.class);
        friendshipEntity.setStatus(FriendshipStatusEnum.DECLINED);
        friendshipRepository.save(friendshipEntity);
    }

    @Override
    @Transactional
    public void deleteFriendship(Long friendId) {
        UserEntity currentUser = userRepository.findById(userService.getCurrentUser().getId())
                .orElseThrow(() -> new UserNotFoundException("Current user not found in database."));
        UserEntity friend = userRepository.findById(friendId)
                .orElseThrow(() -> new UserNotFoundException(friendId.toString()));
        FriendshipKey friendshipKey = new FriendshipKey();
        friendshipKey.setUser(currentUser);
        friendshipKey.setFriend(friend);
        Optional<FriendshipEntity> optionalFriendshipEntity = friendshipRepository.findByFriendshipId(friendshipKey);
        optionalFriendshipEntity.ifPresent(friendshipRepository::delete);
    }

    @Override
    @Transactional
    public void deleteAllFriendshipRelations(Long userId) {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(userId);
        friendshipRepository.deleteAllByFriendshipId_UserOrFriendshipId_Friend(userEntity, userEntity);
    }
}
