package com.egsians.service;

import com.egsians.dto.PostDto;

public interface HashTagService {
    void addHashTags(PostDto postDto);

}
