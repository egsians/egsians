package com.egsians.service;

import com.egsians.dto.FriendshipDto;
import com.egsians.dto.UserDto;
import com.egsians.util.exception.EmptyFriendListException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface FriendshipService {
    Page<UserDto> getFriendsList(Long userId, Pageable pageable);

    Page<UserDto> getFriendshipRequestsToUser(Long userId, Pageable pageable) throws EmptyFriendListException;

    Page<UserDto> getFriendshipRequestsFromUser(Long userId, Pageable pageable);

    Page<UserDto> getRejectedFromUser(Long userId, Pageable pageable);

    Page<UserDto> getRejectsToUser(Long userId, Pageable pageable);

    void sendFriendshipRequest(Long receiverId);

    void acceptInviteFromUser(Long senderId);

    void declineInviteFromUser(Long senderId);

    void deleteFriendship(Long friendId);

    void deleteAllFriendshipRelations(Long userId);
}
