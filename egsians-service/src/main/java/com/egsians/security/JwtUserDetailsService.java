package com.egsians.security;

import com.egsians.dto.UserDto;
import com.egsians.model.UserEntity;
import com.egsians.repository.UserRepository;
import com.egsians.security.jwt.JwtUser;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@Primary
public class JwtUserDetailsService implements UserDetailsService {
    private final UserRepository userRepository;

    public JwtUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) {
        Optional<UserEntity> optionalUserEntity = userRepository.findByEmailIgnoreCase(email);
        UserDto userDto = new UserDto();
        optionalUserEntity.ifPresent(userEntity -> {
            userDto.setId(userEntity.getId());
            userDto.setEmail(email);
            userDto.setPassword(userEntity.getPassword());
        });
        return new JwtUser(userDto);
    }

}
