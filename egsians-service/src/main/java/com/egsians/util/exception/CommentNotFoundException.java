package com.egsians.util.exception;

public class CommentNotFoundException extends RuntimeException {
    CommentNotFoundException(String s) {
        super(s);
    }
}
