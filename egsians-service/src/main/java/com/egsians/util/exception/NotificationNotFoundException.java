package com.egsians.util.exception;

public class NotificationNotFoundException extends RuntimeException {
    public NotificationNotFoundException(String errorMessage) {
        super(errorMessage);
    }
}
