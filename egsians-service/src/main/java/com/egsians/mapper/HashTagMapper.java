package com.egsians.mapper;

import com.egsians.dto.HashTagDto;
import com.egsians.model.HashTagEntity;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

@Component
public class HashTagMapper extends ConfigurableMapper {

    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(HashTagEntity.class, HashTagDto.class)
                .field("id", "id")
                .field("text", "text")
                .byDefault()
                .register();

        factory.classMap(HashTagDto.class, HashTagEntity.class)
                .field("id", "id")
                .field("text", "text")
                .byDefault()
                .register();
    }
}
