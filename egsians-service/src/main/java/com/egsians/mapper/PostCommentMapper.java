package com.egsians.mapper;

import com.egsians.dto.PostCommentDto;
import com.egsians.model.PostCommentEntity;
import com.egsians.model.PostEntity;
import com.egsians.model.UserEntity;
import com.egsians.repository.PostCommentRepository;
import com.egsians.repository.PostRepository;
import com.egsians.repository.UserRepository;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.stream.Collectors;

@Component
@Transactional(readOnly = true)
public class PostCommentMapper extends ConfigurableMapper {
    private final UserRepository userRepo;
    private final PostRepository postRepo;
    private final PostCommentRepository commentRepo;

    public PostCommentMapper(UserRepository userRepo,
                             PostRepository postRepo,
                             PostCommentRepository commentRepo) {
        this.userRepo = userRepo;
        this.postRepo = postRepo;
        this.commentRepo = commentRepo;
    }

    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(PostCommentEntity.class, PostCommentDto.class)
                .field("id", "id")
                .field("postComment", "postComment")
                .field("time", "time")
                .customize(new CustomMapper<PostCommentEntity, PostCommentDto>() {
                    @Override
                    @Transactional
                    public void mapAtoB(PostCommentEntity postCommentEntity, PostCommentDto postCommentDto, MappingContext context) {
                        postCommentDto.setUserId(postCommentEntity.getUser().getId());
                        postCommentDto.setPostId(postCommentEntity.getPost().getId());

                    }
                })
                .register();

        factory.classMap(PostCommentDto.class, PostCommentEntity.class)
                .field("id", "id")
                .field("postComment", "postComment")
                .field("time", "time")
                .byDefault()
                .customize(new CustomMapper<PostCommentDto, PostCommentEntity>() {
                    @Override
                    @Transactional
                    public void mapAtoB(PostCommentDto commentDto, PostCommentEntity commentEntity, MappingContext context) {
                        Optional<UserEntity> optionalUser = userRepo.findById(commentDto.getUserId());
                        optionalUser.ifPresent(commentEntity::setUser);
                        Optional<PostEntity> optionalPost = postRepo.findById(commentDto.getPostId());
                        optionalPost.ifPresent(commentEntity::setPost);
                    }

                    @Override
                    @Transactional
                    public void mapBtoA(PostCommentEntity postCommentEntity, PostCommentDto postCommentDto, MappingContext context) {
                        postCommentDto.setUserId(postCommentEntity.getUser().getId());
                        postCommentDto.setPostId(postCommentEntity.getPost().getId());
                    }
                })
                .register();
    }

}