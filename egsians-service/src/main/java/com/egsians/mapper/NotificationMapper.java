package com.egsians.mapper;

import com.egsians.dto.NotificationDto;
import com.egsians.model.NotificationEntity;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class NotificationMapper extends ConfigurableMapper {
    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(NotificationEntity.class, NotificationDto.class)
                .field("id", "id")
                .field("text", "text")
                .field("date", "date")
                .customize(new CustomMapper<NotificationEntity, NotificationDto>() {
                    @Override
                    @Transactional(readOnly = true)
                    public void mapAtoB(NotificationEntity notificationEntity, NotificationDto notificationDto, MappingContext context) {
                        notificationDto.setUserId(notificationEntity.getUserEntity().getId());
                    }
                }).register();
    }
}
