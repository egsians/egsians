package com.egsians.mapper;

import com.egsians.dto.HashTagDto;
import com.egsians.dto.ImageDto;
import com.egsians.dto.PostDto;
import com.egsians.model.PostEntity;
import com.egsians.model.UserEntity;
import com.egsians.repository.UserRepository;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.stream.Collectors;

@Component
@Transactional(readOnly = true)
public class PostMapper extends ConfigurableMapper {
    private final UserRepository userRepo;
    private final HashTagMapper hashTagMapper;
    private final ImageMapper imageMapper;

    public PostMapper(UserRepository userRepo,
                      HashTagMapper hashTagMapper,
                      ImageMapper imageMapper) {
        this.userRepo = userRepo;
        this.hashTagMapper = hashTagMapper;
        this.imageMapper = imageMapper;
    }

    @Override
    protected void configure(MapperFactory factory) {

        factory.classMap(PostEntity.class, PostDto.class)
                .field("id", "id")
                .fieldAToB("text", "text")
                .customize(new CustomMapper<PostEntity, PostDto>() {
                    @Override
                    @Transactional
                    public void mapAtoB(PostEntity postEntity, PostDto postDto, MappingContext context) {
                        if (postEntity.getImages() != null) {
                            postDto.setImages(postEntity.getImages().stream()
                                    .map(imageEntity -> imageMapper.map(imageEntity, ImageDto.class))
                                    .collect(Collectors.toList()));
                        }
                        if (postEntity.getHashTags() != null) {
                            postDto.setHashTags(postEntity.getHashTags().stream()
                                    .map(hashTagEntity -> hashTagMapper.map(hashTagEntity, HashTagDto.class))
                                    .collect(Collectors.toList()));
                        }
                        postDto.setUserId(postEntity.getUser().getId());
                        postDto.setDate(postEntity.getDate());
                    }

                    @Override
                    public void mapBtoA(PostDto postDto, PostEntity postEntity, MappingContext context) {
                        Optional<UserEntity> optionalUser = userRepo.findById(postDto.getUserId());
                        optionalUser.ifPresent(postEntity::setUser);
                        postEntity.setText(postDto.getText());
                        postEntity.setId(postDto.getId());
                    }
                })
                .register();
    }
}

