package com.egsians.mapper;

import com.egsians.dto.FriendshipDto;
import com.egsians.model.FriendshipEntity;
import com.egsians.model.FriendshipKey;
import com.egsians.model.UserEntity;
import com.egsians.repository.UserRepository;
import com.egsians.util.FriendshipStatusEnum;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Optional;

@Component
public class FriendshipMapper extends ConfigurableMapper {

    private final UserRepository userRepo;

    public FriendshipMapper(UserRepository userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(FriendshipEntity.class, FriendshipDto.class)
                .customize(new CustomMapper<FriendshipEntity, FriendshipDto>() {
                    @Override
                    @Transactional
                    public void mapAtoB(FriendshipEntity friendshipEntity, FriendshipDto friendshipDto, MappingContext context) {
                        friendshipDto.setUserId(friendshipEntity.getFriendshipId().getUser().getId());
                        friendshipDto.setFriendId(friendshipEntity.getFriendshipId().getFriend().getId());
                        friendshipDto.setStatus(friendshipEntity.getStatus().name());
                    }
                })
                .register();

        factory.classMap(FriendshipDto.class, FriendshipEntity.class)
                .byDefault()
                .customize(new CustomMapper<FriendshipDto, FriendshipEntity>() {
                    @Override
                    public void mapAtoB(FriendshipDto friendshipDto, FriendshipEntity friendshipEntity, MappingContext context) {
                        FriendshipKey friendshipKey = new FriendshipKey();
                        Optional<UserEntity> optionalUser = userRepo.findById(friendshipDto.getUserId());
                        optionalUser.ifPresent(friendshipKey::setUser);

                        Optional<UserEntity> optionalFriend = userRepo.findById(friendshipDto.getFriendId());
                        optionalFriend.ifPresent(friendshipKey::setFriend);
                        if (friendshipKey.getUser() != null && friendshipKey.getFriend() != null) {
                            friendshipEntity.setFriendshipId(friendshipKey);
                        }
                        if (StringUtils.isEmpty(friendshipDto.getStatus())) {
                            friendshipEntity.setStatus(FriendshipStatusEnum.WAITING);
                        } else {
                            friendshipEntity.setStatus(FriendshipStatusEnum.valueOf(friendshipDto.getStatus()));
                        }
                    }
                })
                .register();
    }

}

