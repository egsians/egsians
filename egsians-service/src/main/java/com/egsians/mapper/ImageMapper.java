package com.egsians.mapper;

import com.egsians.dto.ImageDto;
import com.egsians.model.ImageEntity;
import com.egsians.model.PostEntity;
import com.egsians.repository.PostRepository;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Component
public class ImageMapper extends ConfigurableMapper {
    private final PostRepository postRepo;

    public ImageMapper(PostRepository postRepo) {
        this.postRepo = postRepo;
    }

    @Override
    protected void configure(MapperFactory factory) {

        factory.classMap(ImageEntity.class, ImageDto.class)
                .field("id", "id")
                .field("imagePath", "imagePath")
                .customize(new CustomMapper<ImageEntity, ImageDto>() {
                    @Override
                    @Transactional
                    public void mapAtoB(ImageEntity imageEntity, ImageDto imageDto, MappingContext context) {
                        imageDto.setPostId(imageEntity.getPost().getId());
                    }
                })
                .register();

        factory.classMap(ImageDto.class, ImageEntity.class)
                .field("id", "id")
                .field("imagePath", "imagePath")
                .byDefault()
                .customize(new CustomMapper<ImageDto, ImageEntity>() {
                    @Override
                    @Transactional
                    public void mapAtoB(ImageDto imageDto, ImageEntity imageEntity, MappingContext context) {
                        Optional<PostEntity> optionalPost = postRepo.findById(imageDto.getPostId());
                        optionalPost.ifPresent(imageEntity::setPost);
                    }
                })
                .register();
    }
}
