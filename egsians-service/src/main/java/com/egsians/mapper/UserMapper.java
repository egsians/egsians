package com.egsians.mapper;

import com.egsians.dto.UserDto;
import com.egsians.model.UserEntity;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

@Component
public class UserMapper extends ConfigurableMapper {

    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(UserDto.class, UserEntity.class)
                .field("id", "id")
                .field("email", "email")
                .fieldAToB("password", "password")//FIXME password is not null when we map entity to dto

                .field("firstName", "firstName")
                .field("lastName", "lastName")
                .customize(new CustomMapper<UserDto, UserEntity>() {
                    @Override
                    public void mapBtoA(UserEntity userEntity, UserDto userDto, MappingContext context) {
                        userDto.setId(userEntity.getId());
                        userDto.setEmail(userEntity.getEmail());
                        userDto.setFirstName(userEntity.getFirstName());
                        userDto.setLastName(userEntity.getLastName());
                    }
                })
                .register();
    }
}
