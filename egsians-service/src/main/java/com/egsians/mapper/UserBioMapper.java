package com.egsians.mapper;

import com.egsians.dto.UserBioDto;
import com.egsians.model.UserBioEntity;
import com.egsians.model.UserEntity;
import com.egsians.repository.UserRepository;
import com.egsians.util.exception.UserNotFoundException;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Component
public class UserBioMapper extends ConfigurableMapper {

    private final UserRepository userRepo;

    public UserBioMapper(UserRepository userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    protected void configure(MapperFactory factory) {

        factory.classMap(UserBioEntity.class, UserBioDto.class)
                .field("id", "id")
                .field("birthday", "birthday")
                .field("summery", "summery")
                .field("mobileNumber", "mobileNumber")
                .field("nickname", "nickname")
                .field("imagePath", "imagePath")
                .customize(new CustomMapper<UserBioEntity, UserBioDto>() {
                    @Override
                    @Transactional
                    public void mapAtoB(UserBioEntity bioEntity, UserBioDto bioDto, MappingContext context) {
                        bioDto.setUserId(bioEntity.getUser().getId());
                    }
                })
                .register();

        factory.classMap(UserBioDto.class, UserBioEntity.class)
                .field("id", "id")
                .field("birthday", "birthday")
                .field("summery", "summery")
                .field("mobileNumber", "mobileNumber")
                .field("nickname", "nickname")
                .field("imagePath", "imagePath")
                .byDefault()
                .customize(new CustomMapper<UserBioDto, UserBioEntity>() {
                    @Override
                    @Transactional
                    public void mapAtoB(UserBioDto bioDto, UserBioEntity bioEntity,
                                        MappingContext context) {
                        Optional<UserEntity> optionalUser = userRepo.findById(bioDto.getUserId());
                        UserEntity userEntity = optionalUser.orElseThrow(
                                () -> new UserNotFoundException(
                                        "User with id " + bioDto.getUserId() + " not found"));
                        bioEntity.setUser(userEntity);
                    }
                }).register();
    }
}
