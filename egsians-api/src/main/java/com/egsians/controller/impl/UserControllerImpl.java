package com.egsians.controller.impl;

import com.egsians.controller.UserController;
import com.egsians.dto.UserBioDto;
import com.egsians.dto.UserDto;
import com.egsians.service.FriendshipService;
import com.egsians.service.UserService;
import com.egsians.util.exception.EmptyFriendListException;
import io.swagger.annotations.Api;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;

@Api
@RestController
@RequestMapping("/api/users")
public class UserControllerImpl implements UserController {

    private final UserService userService;
    private final FriendshipService friendshipService;

    public UserControllerImpl(UserService userService, FriendshipService friendshipService) {
        this.userService = userService;
        this.friendshipService = friendshipService;
    }

    @RequestMapping(value = "/update-user-info",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            method = RequestMethod.PUT)//ok
    public ResponseEntity updateUserInfo(@RequestBody UserDto userDto) {
        userDto.setId(userService.getCurrentUser().getId());
        userService.update(userDto);
        return ResponseEntity.accepted().body(userDto);
    }

    @Override
    @PostMapping(value = "/update-bio")//ok
    public ResponseEntity updateBio(UserBioDto userBioDto, MultipartFile image) throws IOException {
        userBioDto.setUserId(userService.getCurrentUser().getId());
        userService.saveBio(userBioDto, image);
        return ResponseEntity.accepted().body(userBioDto);
    }


    @DeleteMapping("/delete")//ok
    @Override
    public ResponseEntity deleteUser() {
        Long userId = userService.getCurrentUser().getId();
        userService.remove(userId);
        return ResponseEntity.ok(userId);
    }

    @Override
    @GetMapping("/{id}")//ok
    public ResponseEntity findById(@PathVariable Long id) {

        return ResponseEntity.ok(userService.findById(id));
    }

    @Override
    @GetMapping("{id}/get-bio")//ok
    public ResponseEntity getBio(@PathVariable Long id) {
        return ResponseEntity.ok(userService.getBio(id));
    }

    @Override
    @GetMapping("/search")//TODO FIXME make search global
    public ResponseEntity search(
            @RequestParam String searchText,
            @RequestParam(required = false) @DateTimeFormat(pattern = "MM/dd/yyyy") Date from,
            @RequestParam(required = false) @DateTimeFormat(pattern = "MM/dd/yyyy") Date to,
            @PageableDefault(size = 20) Pageable pageable) {

        Page<UserDto> userDtoPage = userService.findByTextPattern(searchText, pageable, from, to);

        return ResponseEntity.ok(userDtoPage);
    }

    @GetMapping("/friends")
    public ResponseEntity getFriends(@PageableDefault(size = 20) Pageable pageable) {
        Page<UserDto> friends = friendshipService.getFriendsList(userService.getCurrentUser().getId(), pageable);
        return ResponseEntity.ok(friends);
    }

    @GetMapping("friends/requests")
    public ResponseEntity getFriendshipInvitesToCurrentUser(@PageableDefault(size = 20) Pageable pageable)
            throws EmptyFriendListException {
        Page<UserDto> friendshipRequestsToUser = friendshipService
                .getFriendshipRequestsToUser(userService.getCurrentUser().getId(), pageable);
        return ResponseEntity.ok(friendshipRequestsToUser);
    }

    @GetMapping("/friends/requests-from-user")
    public ResponseEntity getInvitesFromCurrentUser(@PageableDefault(size = 20) Pageable pageable) {
        Page<UserDto> friendshipRequestsFromUser = friendshipService
                .getFriendshipRequestsFromUser(userService.getCurrentUser().getId(), pageable);
        return ResponseEntity.ok(friendshipRequestsFromUser);
    }

    @PostMapping("/friends/accept")
    public ResponseEntity acceptRequest(@RequestParam("requesterId") Long requesterId) {
        friendshipService.acceptInviteFromUser(requesterId);
        return ResponseEntity.accepted().build();
    }

    @PostMapping("/friends/decline")
    public ResponseEntity declineRequest(@RequestParam("requesterId") Long requesterId) {
        friendshipService.declineInviteFromUser(requesterId);
        return ResponseEntity.accepted().build();
    }

    @PostMapping("/friends/delete")
    public ResponseEntity deleteFriendship(@RequestParam("friendId") Long friendId) {
        friendshipService.deleteFriendship(friendId);
        return ResponseEntity.accepted().build();
    }

    @PostMapping("/friends/send-request")
    public ResponseEntity sendFriendshipRequest(@RequestParam("receiverId") Long receiverId) {
        friendshipService.sendFriendshipRequest(receiverId);
        return ResponseEntity.accepted().build();
    }


}
