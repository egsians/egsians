package com.egsians.controller.impl;

import com.egsians.controller.ImageController;
import com.egsians.dto.ImageDto;
import com.egsians.service.ImageService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/api/images")
public class ImageControllerImpl implements ImageController {

    private final ImageService imageService;

    public ImageControllerImpl(ImageService imageService) {
        this.imageService = imageService;
    }

    @Override
    @GetMapping("/get-image")
    public ResponseEntity<byte[]> getImage(@RequestParam String imagePath) throws IOException {
        return ResponseEntity.ok(imageService.getPictureBytes(imagePath));
    }

    @Override
    @DeleteMapping("/delete-image")
    public ResponseEntity deleteImage(ImageDto imageDto) throws IOException {
        imageService.delete(imageDto);
        return ResponseEntity.ok(imageDto);
    }
}
