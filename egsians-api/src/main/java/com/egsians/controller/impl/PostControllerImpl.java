package com.egsians.controller.impl;

import com.egsians.controller.PostController;
import com.egsians.dto.HashTagDto;
import com.egsians.dto.PostCommentDto;
import com.egsians.dto.PostDto;
import com.egsians.service.ImageService;
import com.egsians.service.PostCommentService;
import com.egsians.service.PostService;
import com.egsians.service.UserService;
import io.swagger.annotations.Api;
import org.mockito.internal.util.StringUtil;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.stream.Stream;

@Api
@RestController
@RequestMapping("/api/posts")
public class PostControllerImpl implements PostController {
    private final PostService postService;
    private final UserService userService;
    private final ImageService imageService;
    private final PostCommentService postCommentService;

    public PostControllerImpl(PostService postService, UserService userService,
                              ImageService imageService, PostCommentService postCommentService) {
        this.postService = postService;
        this.userService = userService;
        this.imageService = imageService;
        this.postCommentService = postCommentService;
    }

    @Override//ok
    @PostMapping(value = "/create", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity createPost(PostDto postDto, MultipartFile[] images) throws URISyntaxException, IOException {
        postDto.setUserId(userService.getCurrentUser().getId());
        Stream.of(images).forEach(image -> {
            try {
                imageService.addForPost(image, postDto);
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        });
        postService.addPost(postDto);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @Override//ok
    @PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updatePost(@RequestBody PostDto postDto) {
        if (postDto.getId() == null || postDto.getUserId() == null) {
            return ResponseEntity.badRequest().build();
        }
        boolean isAuthorized = userService.isUserAuthorized(postService.getOwnerId(postDto));
        if (isAuthorized) {
            postService.updatePost(postDto);
            return ResponseEntity.accepted().body(postDto);
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    @Override//ok
    @DeleteMapping("/delete/{postId}")
    public ResponseEntity deletePost(@PathVariable Long postId) {
        PostDto postDto = new PostDto();
        postDto.setId(postId);
        boolean isAuthorized = userService.isUserAuthorized(postService.getOwnerId(postDto));
        if (isAuthorized) {
            postService.deletePost(postId);
            return ResponseEntity.ok(postId);
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    @Override//ok
    @GetMapping("/{postId}")//ok
    public ResponseEntity findById(@PathVariable Long postId) {
        return ResponseEntity.ok(postService.findPostById(postId));
    }

    @Override//ok
    @GetMapping("/find-by-hashtag")
    public ResponseEntity findByHashTag(@RequestParam String hashTag,
                                        @PageableDefault(size = 20) Pageable pageable) {
        HashTagDto hashTagDto = new HashTagDto();
        hashTagDto.setText(hashTag);
        return ResponseEntity.ok(postService.findAllByHashTag(hashTag, pageable));
    }

    @Override//ok
    @GetMapping("/find-by-user/{userId}")
    public ResponseEntity findByUser(@PathVariable Long userId, Pageable pageable) {
        return ResponseEntity.ok(postService.findAllPostsByUser(userId, pageable));
    }

    @Override
    @GetMapping("/{postId}/comments")//ok
    public ResponseEntity getCommentsOfPost(@PathVariable Long postId,
                                            @PageableDefault(size = 20) Pageable pageable) {
        return ResponseEntity.ok(postCommentService.getCommentsOfPost(postId, pageable));
    }

    @Override
    @GetMapping("/{postId}/comments-count")//ok
    public ResponseEntity getCountOfComments(@PathVariable Long postId) {
        return ResponseEntity.ok(postCommentService.getCountOfCommentsForPost(postId));
    }

    @Override
    @PostMapping(value = "/{postId}/add-comment", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity addComment(@PathVariable Long postId, @RequestBody PostCommentDto postCommentDto) {
        postCommentDto.setUserId(userService.getCurrentUser().getId());
        postCommentDto.setPostId(postId);
        if (StringUtils.isEmpty(postCommentDto.getPostComment())) {
            return ResponseEntity.badRequest().build();
        }
        postCommentService.addCommentToPost(postCommentDto);
        return ResponseEntity.ok().build();
    }


    @Override
    @GetMapping("/comments/{commentId}/replies")//FIXME
    public ResponseEntity getRepliesOfComment(@PathVariable Long commentId,
                                              @PageableDefault(size = 20) Pageable pageable) {
        return ResponseEntity.ok(postCommentService.getReplies(commentId, pageable));
    }
}
