package com.egsians.controller.impl;

import com.egsians.controller.NotificationController;
import com.egsians.dto.NotificationDto;
import com.egsians.service.NotificationService;
import com.egsians.service.UserService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/notifications")
public class NotificationControllerImpl implements NotificationController {
    private final NotificationService notificationService;
    private final UserService userService;

    public NotificationControllerImpl(NotificationService notificationService, UserService userService) {
        this.notificationService = notificationService;
        this.userService = userService;
    }
    @Override
    @GetMapping("")
    public ResponseEntity getNotificationsFromConcreteUser(@RequestParam(name = "userId", required = false) Long userId,
                                                           @PageableDefault(size = 20) Pageable pageable) {

        Boolean isFriends = userService.isFriends(userId, userService.getCurrentUser().getId());
        if (isFriends) {
            Page<NotificationDto> notificationsFromConcreteUser =
                    notificationService.getNotificationsFromConcreteUser(userId, pageable);
            return ResponseEntity.ok(notificationsFromConcreteUser);
        }
        Page<NotificationDto> notificationsForUser =
                notificationService.getNotificationsForUser(userService.getCurrentUser().getId(), pageable);
        return ResponseEntity.ok(notificationsForUser);
    }

    @Override
    @GetMapping("/")
    public ResponseEntity getNotificationsForUser(@PageableDefault(size = 20) Pageable pageable) {
        Page<NotificationDto> notificationsForUser =
                notificationService.getNotificationsForUser(userService.getCurrentUser().getId(), pageable);
        return ResponseEntity.ok(notificationsForUser);
    }

    @Override
    @PostMapping("/{notificationId}/set-seen")
    public ResponseEntity setNotificationSeen(@PathVariable Long notificationId) {
        notificationService.setSeen(userService.getCurrentUser().getId(), notificationId);
        return ResponseEntity.ok().build();
    }
}
