package com.egsians.controller;

import com.egsians.dto.ImageDto;
import org.springframework.http.ResponseEntity;

import java.io.IOException;

public interface ImageController {

    public ResponseEntity<byte[]> getImage(String imagePath) throws IOException;
    ResponseEntity deleteImage(ImageDto imageDto) throws IOException;
}

