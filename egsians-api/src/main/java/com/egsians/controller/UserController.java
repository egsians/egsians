package com.egsians.controller;

import com.egsians.dto.UserBioDto;
import com.egsians.dto.UserDto;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;

public interface UserController {

    ResponseEntity updateUserInfo(UserDto user);

    ResponseEntity updateBio(UserBioDto bio, MultipartFile image) throws IOException;

    ResponseEntity deleteUser();

    ResponseEntity findById(Long id);

    ResponseEntity getBio(Long id);

    ResponseEntity search(String searchText,
                          Date from,
                          Date to,
                          Pageable pageable);

}
