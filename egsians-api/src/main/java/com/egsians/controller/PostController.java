package com.egsians.controller;

import com.egsians.dto.PostCommentDto;
import com.egsians.dto.PostDto;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URISyntaxException;


public interface PostController {

    ResponseEntity createPost(PostDto postDto, MultipartFile[] images) throws URISyntaxException, IOException;

    ResponseEntity updatePost(PostDto postDto);

    ResponseEntity deletePost(Long id);

    ResponseEntity findById(Long id);

    ResponseEntity findByHashTag(String hashTag, Pageable pageable);

    ResponseEntity findByUser(Long userId, Pageable pageable);

    ResponseEntity getCommentsOfPost(Long postId, Pageable pageable);

    ResponseEntity getRepliesOfComment(Long commentId, Pageable pageable);

    ResponseEntity getCountOfComments(Long postId);

    ResponseEntity addComment(Long postId, PostCommentDto postCommentDto);
}
