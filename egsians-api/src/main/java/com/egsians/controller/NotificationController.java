package com.egsians.controller;

import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

public interface NotificationController {

    ResponseEntity getNotificationsFromConcreteUser(Long userId, Pageable pageable);

    ResponseEntity getNotificationsForUser(Pageable pageable);

    ResponseEntity setNotificationSeen(Long id);

}
