package com.egsians.controller.auth;

import com.egsians.dto.AuthenticationRequestDto;
import com.egsians.dto.UserDto;
import org.springframework.http.ResponseEntity;

public interface AuthenticationController {
    ResponseEntity register(UserDto userDto);

    ResponseEntity login(AuthenticationRequestDto authenticationRequestDto);
}
