package com.egsians.controller.auth;

import com.egsians.dto.AuthenticationRequestDto;
import com.egsians.dto.UserDto;
import com.egsians.security.jwt.JwtTokenProvider;
import com.egsians.security.jwt.JwtUser;
import com.egsians.service.UserService;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@Api
@RestController
@RequestMapping("/api/auth")
public class AuthenticationControllerImpl implements AuthenticationController {
    private final AuthenticationManager authenticationManager;
    private final UserService userService;
    private final JwtTokenProvider jwtTokenProvider;


    public AuthenticationControllerImpl(AuthenticationManager authenticationManager, UserService userService, JwtTokenProvider jwtTokenProvider) {
        this.authenticationManager = authenticationManager;
        this.userService = userService;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Override
    @PostMapping(value = "/register")
    public ResponseEntity register(@RequestBody UserDto userDto) {
        userDto = userService.save(userDto);
        String token = jwtTokenProvider.createToken(userDto.getId(), userDto.getEmail());
        Map<Object, Object> response = new HashMap<>();
        response.put("username", userDto.getEmail());
        response.put("token", token);
        return ResponseEntity.ok(response);
    }

    @Override
    @PostMapping(value = "/login")
    public ResponseEntity login(@RequestBody AuthenticationRequestDto authenticationRequestDto) {
        String email = authenticationRequestDto.getEmail();
        JwtUser jwtUser = (JwtUser) authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        email,
                        authenticationRequestDto.getPassword())).getPrincipal();

        String token = jwtTokenProvider.createToken(jwtUser.getId(), jwtUser.getUsername());

        Map<Object, Object> response = new HashMap<>();
        response.put("username", email);
        response.put("token", token);

        return ResponseEntity.ok(response);
    }
}
