package com.egsians.repository;

import com.egsians.model.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Arrays;
import java.util.Date;

@Repository
@Transactional(readOnly = true)
public class UserRepositoryCustomImpl implements UserRepositoryCustom {

    private final EntityManager entityManager;


    public UserRepositoryCustomImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * No comments, no justifications, just trust us:))
     *
     * @param names    String array that contains names
     * @param from     Date for search by age
     * @param to       Date for search by age
     * @param pageable Pagination realisation
     * @return Page of UserEntities
     */
    @Override
    public Page<UserEntity> findAllByNameAndAge(String[] names, Date from, Date to, Pageable pageable) {
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("select DISTINCT user.id,  user.first_name," +
                " user.last_name, user.email, user.password ")
                .append("from users as user,  user_bio as bio ")
                .append("where bio.user_id = user.id and (");
        Arrays.stream(names).forEach(name -> queryBuilder.append("(user.first_name like '%").append(name).append("%' AND user.last_name like '%").append(name).append("%') OR "));
        Arrays.stream(names).forEach(name -> queryBuilder.append("(user.first_name like '%").append(name).append("%') OR "));
        Arrays.stream(names).forEach(name -> queryBuilder.append("(user.last_name like '%").append(name).append("%') OR "));
        queryBuilder.delete(queryBuilder.lastIndexOf("OR"), queryBuilder.lastIndexOf("OR") + 2);
        queryBuilder.append(")");
        if (from != null) {
            queryBuilder.append("AND bio.birthday > '" +
                    from.toInstant().toString().substring(0, 10) + "' ");
        }
        if (to != null) {
            queryBuilder.append("AND bio.birthday < '" +
                    to.toInstant().toString().substring(0, 10) + "' ");
        }
        String countQuery = queryBuilder.toString();
        int totalCount = entityManager.createNativeQuery(countQuery).getResultList().size();

        int count = pageable.getPageSize();
        int startIndex = pageable.getPageNumber() * count;
        queryBuilder.append(" LIMIT " + startIndex + "," + count);
        Query nativeQuery = entityManager.createNativeQuery(queryBuilder.toString(), UserEntity.class);
        return new PageImpl<>(nativeQuery.getResultList(), pageable, totalCount);
    }
}
