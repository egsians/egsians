package com.egsians.repository;

import com.egsians.model.NotificationEntity;
import com.egsians.model.NotificationsReceiversEntity;
import com.egsians.model.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface NotificationsReceiversRepository extends JpaRepository<NotificationsReceiversEntity, Long> {
    Page<NotificationsReceiversEntity> findAllById(Long id, Pageable pageable);

    Page<NotificationsReceiversEntity> findAllByUserEntityAndIsSeen(UserEntity userEntity, Boolean isSeen,
                                                                    Pageable pageable);

    Optional<NotificationsReceiversEntity> findByUserEntityAndNotificationEntity(UserEntity userEntity,
                                                                                 NotificationEntity notificationEntity);

}
