package com.egsians.repository;

import com.egsians.dto.UserDto;
import com.egsians.model.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;

public interface UserRepositoryCustom {
    Page<UserEntity> findAllByNameAndAge(String[] names, Date from, Date to, Pageable pageable);

}
