package com.egsians.repository;

import com.egsians.model.HashTagEntity;
import com.egsians.model.PostEntity;
import com.egsians.model.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepository extends JpaRepository<PostEntity, Long> {

    Page<PostEntity> findAllByUser(UserEntity user, Pageable pageable);

    Page<PostEntity> findAllByHashTags(HashTagEntity hashTag, Pageable pageable);
}
