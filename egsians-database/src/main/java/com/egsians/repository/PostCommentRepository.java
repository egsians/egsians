package com.egsians.repository;

import com.egsians.model.PostCommentEntity;
import com.egsians.model.PostEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PostCommentRepository extends JpaRepository<PostCommentEntity, Long> {

    @Query(value = "SELECT comment from PostCommentEntity comment where comment.post=:post and comment.parent is null ")
    Page<PostCommentEntity> findAllByPost(@Param("post") PostEntity postEntity, Pageable pageable);

}
