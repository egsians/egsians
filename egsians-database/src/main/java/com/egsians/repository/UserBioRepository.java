package com.egsians.repository;

import com.egsians.model.UserBioEntity;
import com.egsians.model.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.Optional;

@Repository
public interface UserBioRepository extends JpaRepository<UserBioEntity, Long> {
    Optional<UserBioEntity> findByUser(UserEntity user);

    Page<UserBioEntity> findAllByBirthdayGreaterThanEqualAndBirthdayLessThanEqual(
            Date startDate, Date endDate, Pageable pageable);

    Optional<UserBioEntity> findByMobileNumber(String mobileNumber);


}
