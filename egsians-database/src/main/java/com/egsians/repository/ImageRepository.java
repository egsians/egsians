package com.egsians.repository;

import com.egsians.model.ImageEntity;
import com.egsians.model.PostEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImageRepository extends JpaRepository<ImageEntity, Long> {

    Page<ImageEntity> getAllByPost(PostEntity post, Pageable pageable);

}
