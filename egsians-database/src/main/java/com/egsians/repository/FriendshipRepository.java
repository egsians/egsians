package com.egsians.repository;

import com.egsians.model.FriendshipEntity;
import com.egsians.model.FriendshipKey;
import com.egsians.model.UserEntity;
import com.egsians.util.FriendshipStatusEnum;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FriendshipRepository extends JpaRepository<FriendshipEntity, Long> {

    Optional<FriendshipEntity> findByFriendshipId(FriendshipKey friendshipKey);

    /**
     * get all friendship entities, where part of PK is our user, and status is ACCEPTED
     *
     * @param status   for checking friendship relation type
     * @param user     our user
     * @param sameUser user can be in different parts in PK
     * @param pageable for pagination
     * @return Page<FriendshipEntity> where have  our user
     * @see FriendshipStatusEnum
     */
    Page<FriendshipEntity> findAllByStatusAndFriendshipId_UserOrFriendshipId_Friend(FriendshipStatusEnum status,
                                                                                    UserEntity user,
                                                                                    UserEntity sameUser,
                                                                                    Pageable pageable);

    /**
     * with this method we can get all friendship relations, where someone send request to our user
     *
     * @param status   for checking friendship relation type
     * @param user     our UserEntity
     * @param pageable for pagination
     * @return Page<FriendshipEntity>
     * @see FriendshipStatusEnum
     * @see Pageable
     */
    Page<FriendshipEntity> findByStatusAndFriendshipId_Friend(FriendshipStatusEnum status,
                                                              UserEntity user,
                                                              Pageable pageable);

    Page<FriendshipEntity> findByStatusAndFriendshipId_User(FriendshipStatusEnum statusEnum,
                                                            UserEntity user,
                                                            Pageable pageable);

    void deleteAllByFriendshipId_UserOrFriendshipId_Friend(UserEntity user, UserEntity sameUser);

}
